function changeColor (contenedor, changeData) {
  const seleccion = document.querySelector('.seleccion');
  contenedor.addEventListener("change" , e => {
    const color = e.target.id;
    changeData(color)
    switch (color) {
      case 'color-1':
        seleccion.classList.remove('violet')
        seleccion.classList.remove('green')
        seleccion.classList.remove('red')
        seleccion.classList.add('blue')        
        break;

      case 'color-2':
        seleccion.classList.remove('blue')
        seleccion.classList.remove('violet')
        seleccion.classList.remove('green')
        seleccion.classList.add('red')
        break;

      case 'color-3':
        seleccion.classList.remove('blue')
        seleccion.classList.remove('red')
        seleccion.classList.remove('green')
        seleccion.classList.add('violet')
        break;

      case 'color-4':
        seleccion.classList.remove('blue')
        seleccion.classList.remove('red')
        seleccion.classList.remove('violet')
        seleccion.classList.add('green')
        break;

      default:
        seleccion.classList.add('blue')
        break;
    }
  })
}

export { changeColor }