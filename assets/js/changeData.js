const dataArray = [{id: 'color-1',title: 'Blue', parrafo: 'The Blue color is my favorite color', money:'$1.900'}, {id: 'color-2',title: 'Red', parrafo: 'The Red color is my favorite color', money:'$2.900'}, {id: 'color-3',title: 'Violet', parrafo: 'The Violet color is my favorite color', money:'$3.900'}, {id: 'color-4',title: 'Green', parrafo: 'The Green color is my favorite color', money:'$5.000'}]

function changeData (color) {
  const data = document.querySelector('.information');
  const elements = data.children;
  const [...items] = elements;
  items.map(item => {
    let classItem = item.className
    dataArray.map(({id, title, parrafo, money}) => {
      if(color === id) {
        if(classItem === 'title')
          item.textContent = title
        if(classItem === 'description')
          item.textContent = parrafo
        if(classItem === 'money')
          item.textContent = money
      }
    })
  });
}

export { changeData };