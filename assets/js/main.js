import { changeColor } from "./changeColor.js"
import { changeData } from "./changeData.js"

const contenedor = document.getElementById('contenedor');

document.addEventListener("DOMContentLoaded", e => {
  changeColor(contenedor, changeData)
  changeData('color-1')
})